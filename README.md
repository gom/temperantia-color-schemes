# temperantia color schemes

Color schemes using the temperantia palette, for [GNOME Terminal](gnome-terminal/), [GtkSourceview](GtkSourceView/), and [Tilix](tilix/).  See each directory for information, install instructions, etc.

