# temperantia color scheme for Tilix

**Appx reading time: < 1 minute**

The Tilix color scheme is made to be identical with the temperantia GNOME Terminal color scheme (but might differ slightly in terms of what colors are used where). It's installed by simply copying the `temperantia.json` file to Tilix' root `schemes` directory - see below.  

# Installation

**1.** Open a terminal inside of the directory `temperantia-color-schemes/tilix`.  

**2.** Then run the following:  

```
sudo cp temperantia.json /usr/share/tilix/schemes  
```

**3.** Now go to Tilix' **preferences**. In the sidebar, first select the profile you want to use/are using, then select the "Colors" tab and choose "temperantia" from the "Color Scheme" menu.  
