# temperantia GtkSourceView color scheme

**Appx. reading time: 1 minute**  

The temperantia color scheme for GtkSourceView is a dark color scheme using the temperantia color palette. It can be used in applications like Gedit, Builder, Meld, Mousepad etc.  

![Screenshot of Gedit using the temperantia color scheme](images/temperantia-gtksourceview.png)  

# Installation

The color scheme can be installed for a local user only, or for system wide use. Instructions for both of these are below.  

## Local installation

**1.** Open a terminal inside of the directory `temperantia-color-schemes/GtkSourceView`.  

**2.** Then run the following to, firstly, create the directories you need, and then copy the `temperantia.xml` file to each of these (if the directories already exist, the XML file will just be copied to them):  

```
mkdir -p ~/.local/share/gtksourceview-4/styles ~/.local/share/gtksourceview-3.0/styles; cp temperantia.xml ~/.local/share/gtksourceview-4/styles; cp temperantia.xml ~/.local/share/gtksourceview-3.0/styles  
```

**3.** Now you should be able to choose the temperantia color scheme in the preferences section of relevant applications.  

## System wide installation

**1.** Open a terminal inside of the directory `temperantia-color-schemes/GtkSourceView`.  

**2.** Then run the following to copy the `temperantia.xml` file to GtkSourceView's root `styles` directory:  

```
sudo cp temperantia.xml /usr/share/gtksourceview-4/styles  
```

**3.** Now you should be able to choose the temperantia color scheme in the preferences section of relevant applications.  

**4.** However, while most applications (in the Debian 11 repositories) use GtkSourceView version 4, some use version 3 (e.g. Meld). If you're not able to choose the temperantia color scheme in a GTK application with support for syntax highlighting, the reason might be that it only supports GtkSourceView 3. If so, you can try copying the `temperantia.xml` file to yet another directory, by running the following:  

```
sudo cp temperantia.xml /usr/share/gtksourceview-3.0/styles  
```
