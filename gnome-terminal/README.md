# temperantia color scheme for GNOME Terminal

**Appx. reading time: 1 minute**  

The temperantia color scheme for GNOME Terminal is primarily intended to be used together with the temperantia GTK theme. However, it is of course possible to use only the color scheme, without using the GTK theme (and vice versa).  

The color scheme is installed by running a script that creates a profile called "temperantia", which you then can choose in the GNOME Terminal preferences. See further below.  

![Screenshot of GNOME Terminal using the temperantia color scheme](images/temperantia-gnome-terminal.png)  

# Installation

Before the actual installation, three minor **preparatory** steps are needed.  

**1.** Firstly, the package `uuid-runtime` has to be installed (if it isn't already), since it's required to make the script work.  

> **NOTE**: Depending on the distribution, this package might have another name.  

**2.** Second, a GNOME Terminal profile has to be set as the **default**. It needs to be one you create yourself - the profile that's the default, *by* default, isn't enough. It doesn't matter what you name the profile you create or what settings it has, it just needs to be there and it needs to be set as the default.  

**3.** And third, you need to allow the `temperantia.sh` file to be executed as a program. This can be done by right clicking the `temperantia.sh` file, choosing "Properties", then selecting the "Permissions" tab, and lastly checking the box that says "Allow executing file as program".  

**4.** Now you should be ready to **install** the color scheme. To do so, open a terminal inside of the directory `temperantia-color-schemes/gnome-terminal`. Then run the following to execute the script:  

```
./temperantia.sh  
```

**5.** After the script has been executed, open the GNOME Terminal **preferences**. You should see a new profile named "temperantia" in the sidebar. Make it the **default** profile, and the color scheme should now be active and work as intended. You may need to close and relaunch GNOME Terminal, though, in order for the changes to take effect.  

**6.** If something goes wrong, for example if you by mistake change one of the colors, simply redo steps **4** and **5**. In general it's possible to run the script as many times as you like; the script will simply create a *new* profile named "temperantia" every time you run it. It will *not* overwrite any existing profile or settings.  
