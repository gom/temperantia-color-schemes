#!/usr/bin/env bash
# temperantia color scheme install script for Gnome Terminal
#
# Based on https://github.com/aarowill/base16-gnome-terminal/ 
# licensed under the MIT License, copyright 2017 Aaron Williamson
#
# All modifications licensed under the GNU General Public License, 
# v3.0 or later, copyright 2022 gom
# <gom@disroot.org>
# https://codeberg.org/gom/

[[ -z "$PROFILE_NAME" ]] && PROFILE_NAME="temperantia"
[[ -z "$PROFILE_SLUG" ]] && PROFILE_SLUG="temperantia"
[[ -z "$DCONF" ]] && DCONF=dconf
[[ -z "$UUIDGEN" ]] && UUIDGEN=uuidgen

dset() {
    local key="$1"; shift
    local val="$1"; shift

    if [[ "$type" == "string" ]]; then
        val="'$val'"
    fi

    "$DCONF" write "$PROFILE_KEY/$key" "$val"
}

# because dconf doesn't have "append"
dlist_append() {
    local key="$1"; shift
    local val="$1"; shift

    local entries="$(
        {
            "$DCONF" read "$key" | tr -d '[]' | tr , "\n" | fgrep -v "$val"
            echo "'$val'"
        } | head -c-1 | tr "\n" ,
    )"

    "$DCONF" write "$key" "[$entries]"
}

# newest versions of gnome terminal use dconf
if which "$DCONF" > /dev/null 2>&1; then

    # check that uuidgen is available
    type $UUIDGEN >/dev/null 2>&1 || { echo >&2 "Requires uuidgen but it's not installed - aborting! Please install uuid-runtime and try again."; exit 1; }
    
    [[ -z "$BASE_KEY_NEW" ]] && BASE_KEY_NEW=/org/gnome/terminal/legacy/profiles:

    if [[ -n "`$DCONF list $BASE_KEY_NEW/`" ]]; then
        if which "$UUIDGEN" > /dev/null 2>&1; then
            PROFILE_SLUG=`uuidgen`
        fi

        if [[ -n "`$DCONF read $BASE_KEY_NEW/default`" ]]; then
            DEFAULT_SLUG=`$DCONF read $BASE_KEY_NEW/default | tr -d \'`
        else
            DEFAULT_SLUG=`$DCONF list $BASE_KEY_NEW/ | grep '^:' | head -n1 | tr -d :/`
        fi

        DEFAULT_KEY="$BASE_KEY_NEW/:$DEFAULT_SLUG"
        PROFILE_KEY="$BASE_KEY_NEW/:$PROFILE_SLUG"

        # copy existing settings from default profile
        $DCONF dump "$DEFAULT_KEY/" | $DCONF load "$PROFILE_KEY/"

        # add new copy to list of profiles
        dlist_append $BASE_KEY_NEW/list "$PROFILE_SLUG"

        # update gnome terminal profile w/ temperantia colors & settings
        dset visible-name "'$PROFILE_NAME'"
        dset use-theme-colors "false"
        dset background-color "'#313236'"
        dset foreground-color "'#bbbbcc'"
        dset bold-color "'#bbbbcc'"
        dset bold-color-same-as-fg "true"
        dset cursor-colors-set "true"
	dset cursor-background-color "'#bbbbcc'"
	dset cursor-foreground-color "'#313236'"
        dset highlight-colors-set "true"
	dset highlight-background-color "'#3c8291'"
	dset highlight-foreground-color "'#f0f0f0'"
        dset palette "[
        
        '#313236',
        '#e6915a',
        '#d7799c',
        '#eccc6a',
        '#4a9cb0',
        '#58b7b7',
        '#b2a8f0',
        '#dadada',
        '#413c41',
        '#df803f',
        '#e78dae',
        '#e2b11e',
        '#81c4d5',
        '#73c4c4',
        '#8eb8e6',
        '#bbbbcc'
        
        ]"

        unset PROFILE_NAME
        unset PROFILE_SLUG
        unset DCONF
        unset UUIDGEN
        exit 0
    fi
fi

